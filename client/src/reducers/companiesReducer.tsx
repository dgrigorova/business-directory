import { FETCH_COMPANIES } from '../common/constants';

type Action = {
    payload: string[],
    type: string
}

const companiesReducer = (state = [], action: Action) => {
    switch (action.type) {
        case FETCH_COMPANIES:
            return action.payload;
        default:
            return state;
    }
};

export default companiesReducer;