import { FETCH_COMPANIES } from '../common/constants';
import { AppDispatch } from '../reducers';
import companyData from '../apis/companyData';

export const fetchCompanies = () => {
    return async (dispatch: AppDispatch) => {
        await companyData.get('/')
            .then((response) => dispatch({ type: FETCH_COMPANIES, payload: response.data }))
            .catch((error) => console.log(error.message));
    };
};