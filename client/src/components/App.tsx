import { Routes, Route, Navigate, useNavigate } from 'react-router-dom';

import './App.css';
import Navbar from './Base/Navbar/Navbar';
import Container from './Base/Container/Container';
import CompanyList from './Pages/CompanyList/CompanyList';
import CompanyDetails from './Pages/CompanyDetails/CompanyDetails';

const App = () => {
    const navigate = useNavigate();

    return (
        <>
            <Navbar />
            <Container>
                <Routes>
                    <Route path="/" element={<Navigate to="/companies" replace />} />
                    <Route path="/companies" element={<CompanyList navigate={navigate} />} />
                    <Route path="/companies/:companyName" element={<CompanyDetails />} />
                </Routes>
            </Container>
        </>
    )
}

export default App;