type ContainerProps = {
    children: JSX.Element | JSX.Element[]
}

const Container = ({ children }: ContainerProps) => {
    return (
        <div className="main-container">
            {children}
        </div>
    )
};

export default Container;