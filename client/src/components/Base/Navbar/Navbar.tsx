import React from 'react';
import { Link } from 'react-router-dom';

import './Navbar.css';

const Navbar: React.FC = () => {
    const logo = require('../../../assets/images/logo.png');

    return (
        <Link to="/companies">
            <div className="navbar">
                <img className='logo' alt={'logo'} src={logo} />
            </div>
        </Link>
    )
};

export default Navbar;