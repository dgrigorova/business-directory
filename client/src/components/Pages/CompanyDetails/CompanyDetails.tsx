import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';

import './CompanyDetails.css';
import { ICompany } from '../../../models/company';
import { RootState } from './../../../reducers/index';

type Props = {
    companies: ICompany[];
}

const CompanyDetails = (props: Props) => {

    const { companyName } = useParams();
    const selectedCompany = props.companies.find(company => company.name === companyName);

    if (!selectedCompany) {
        return <p>Loading...</p>;
    }

    const nearbyCompanies = props.companies.filter(company => company.address.city === selectedCompany.address.city);

    const renderNearbyCompanies = () => {
        return (
            nearbyCompanies.map(company => {
                return (
                    <tr key={company.id}>
                        <td>{company.name}</td>
                        <td>{company.address.number} {company.address.street}, {company.address.city} {company.address.zip}</td>
                    </tr>
                )
            })
        )
    }

    const renderCompanyDetailsContainer = () => {
        return (
            <div id='company-details-container'>
                <div className='company-details-image-container'>
                    <img alt={selectedCompany.name} src={selectedCompany.image} className='image' />
                </div>
                <div className='company-details-content'>
                    <div className='details-card'>
                        <h1>Address</h1>
                        <p>{`${selectedCompany.address.number} ${selectedCompany.address.street}`}</p>
                        <p>{`${selectedCompany.address.city}, ${selectedCompany.address.zip}`}</p>
                    </div>
                    <div className='details-card'>
                        <h1>Contact</h1>
                        <p>{selectedCompany.phone}</p>
                        <p>{selectedCompany.email}</p>
                    </div>
                    <div id='nearby-places-card'>
                        <h1>Nearby Places</h1>
                        <table id='nearby-places-table'>
                            <tbody>
                                {renderNearbyCompanies()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }

    return renderCompanyDetailsContainer();
};

const mapStateToProps = (state: RootState) => {
    return {
        companies: state.companies,
    }
};

export default connect(mapStateToProps)(CompanyDetails);