import React, { ReactNode } from 'react';
import { connect } from 'react-redux';

import './CompanyList.css';
import { fetchCompanies } from '../../../actions/index';
import { ICompany } from '../../../models/company';
import { RootState } from './../../../reducers/index';

type Props = {
    companies: ICompany[];
    fetchCompanies: Function;
    navigate: Function;
}

class CompanyList extends React.Component<Props> {

    componentDidMount() {
        if (this.props.companies.length === 0) {
            this.props.fetchCompanies();
        }
    }

    onCompanySelect = (company: ICompany) => {
        this.props.navigate(`companies/${company.name}`);
    }

    renderList() {
        return this.props.companies.map(company => {
            return (
                <tr key={company.id} onClick={() => this.onCompanySelect(company)}>
                    <td>{company.name}</td>
                    <td>{company.description}</td>
                </tr>
            )
        })
    }

    render(): ReactNode {
        return (
            <div className='table-container'>
                <table id='company-table'>
                    <thead>
                        <tr>
                            <th>NAME</th>
                            <th>DESCRIPTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderList()}
                    </tbody>
                </table>
            </div>
        );
    }
};

const mapStateToProps = (state: RootState) => {
    return {
        companies: state.companies,
    }
}

export default connect(mapStateToProps, {
    fetchCompanies,
})(CompanyList);